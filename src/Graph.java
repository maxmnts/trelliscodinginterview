import java.util.*;

public class Graph {
    // Implement adjacency list
    HashMap<String, ArrayList<EdgeNode>> edges;
    Integer numberOfEdges = 0;
    Integer numberOfVertices = 0;

    public Graph() {
        edges = new HashMap<>();
    }

    public void addConnection(String a, String b, Double distance, boolean bidirection) {

        if (edges.containsKey(a)) {
            // If edgenode does not exist in list, then add it
            boolean foundInList = false;

            for (EdgeNode node : edges.get(a)) {
                // If edgenode does exist and it has a different distance, then update edgenode
                foundInList = node.getNext().contentEquals(b);
                if (foundInList && !node.getDistance().equals(distance)) {
                    node.setDistance(distance);
                }
            }

            if (!foundInList) {
                edges.get(a).add(new EdgeNode(b, distance));
            }
        } else {
            ArrayList<EdgeNode> list = new ArrayList<>();
            list.add(new EdgeNode(b, distance));
            edges.put(a, list);
            numberOfVertices += 1;
        }

        if (bidirection) {
            addConnection(b, a, distance, false);
        } else {
            numberOfEdges += 1;
        }
    }

    public Double computeShortestDistance(String source, String target) {
        // Dijkstra's Algorithm
        HashMap<String, Double> distanceFromStart = new HashMap<>();
        String currentNodeToProcess;
        String candidateNextVertex;
        Double edgeDistance;
        Double bestCurrentDistance;
        HashMap<String, Boolean> inTree = new HashMap<>();
        HashMap<String, String> parent =  new HashMap<>();

        // Initialize nodes
        edges.forEach((s, edgeNodes) -> {
            distanceFromStart.put(s, Double.MAX_VALUE);
            inTree.put(s, false);
            parent.put(s, null);
        });

        distanceFromStart.replace(source, 0d);
        currentNodeToProcess = source;

        while (!inTree.get(currentNodeToProcess)) {
            inTree.replace(currentNodeToProcess, true);
            for (EdgeNode node : edges.get(currentNodeToProcess)) {
                candidateNextVertex = node.getNext();
                edgeDistance = node.getDistance();
                if (distanceFromStart.get(candidateNextVertex) > (distanceFromStart.get(currentNodeToProcess) + edgeDistance)) {
                    distanceFromStart.replace(candidateNextVertex, distanceFromStart.get(currentNodeToProcess) + edgeDistance);
                    parent.replace(candidateNextVertex, currentNodeToProcess);
                }
            }
        }

        if (distanceFromStart.get(target).equals(Double.MAX_VALUE)) {
            bestCurrentDistance = -9999d;
        } else {
            bestCurrentDistance = distanceFromStart.get(target);
        }

        return bestCurrentDistance;
    }
}

class EdgeNode {

    private String next;
    private Double distance;

    public EdgeNode(String b, Double distance) {
        this.next = b;
        this.distance = distance;
    }

    public String getNext() {
        return next;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

}
