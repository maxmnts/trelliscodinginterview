

public class DistanceOptimizer implements IDistanceOptimizer {
    
    private Graph graph;

    public DistanceOptimizer() {
        graph = new Graph();
    }

    @Override
    public void addConnection(String a, String b, Double distance, boolean bidirection) {
        graph.addConnection(a, b, distance, bidirection);
    }

    @Override
    public Double computeShortestDistance(String a, String b) {
        return graph.computeShortestDistance(a, b);
    }
}
