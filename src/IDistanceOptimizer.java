public interface IDistanceOptimizer {
    void addConnection(String a, String b, Double distance, boolean bidirection);
    Double computeShortestDistance(String a, String b);
}