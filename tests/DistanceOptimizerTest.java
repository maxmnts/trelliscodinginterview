import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DistanceOptimizerTest {
    private IDistanceOptimizer optimizer = new DistanceOptimizer();


    @BeforeEach
    void setupData() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("data.txt").getFile());

        try (Stream<String> stream = Files.lines(Paths.get(file.toURI()))) {
            stream.forEach(s -> {
                List<String> splitDataList = Arrays.asList(s.split(","));

                String a = splitDataList.get(0);
                String b = splitDataList.get(1);
                Double distance = Double.parseDouble(splitDataList.get(2));
                boolean bidirection = splitDataList.get(3).contentEquals("y");

                // Ideally we would validate both line and values before adding a connection.
                optimizer.addConnection(a, b, distance, bidirection);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    void testShortDistanceAG() {

        assertEquals(9d, optimizer.computeShortestDistance("a", "g"), 0.00001d);

    }



    @Test
    void testShortDistanceGH() {

        assertEquals(12d, optimizer.computeShortestDistance("g", "h"), 0.00001d);

    }



    @Test
    void testShortDistanceZA() {

        assertEquals(-9999d, optimizer.computeShortestDistance("z", "a"), 0.00001d);

    }



    @Test
    void testShortDistanceBE() {

        assertEquals(6d, optimizer.computeShortestDistance("b", "e"), 0.00001d);

    }



    @Test
    void testShortDistanceAK() {

        assertEquals(4d, optimizer.computeShortestDistance("a", "k"), 0.00001d);

    }



    @Test
    void testShortDistanceAI() {

        assertEquals(9d, optimizer.computeShortestDistance("a", "i"), 0.00001d);

    }



    @Test
    void testShortDistanceIA() {

        assertEquals(9d, optimizer.computeShortestDistance("i", "a"), 0.00001d);

    }
}
